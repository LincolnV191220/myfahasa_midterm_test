import 'package:flutter/material.dart';
import 'package:myfahasa/size_config.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key? key,
    required this.text,
    required this.press,
  }) : super(key: key);

  final String text;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: TextStyle(
                  fontSize: getProportionateScreenHeight(18),
                  color: Colors.black),
            ),
            GestureDetector(
              onTap: press,
              child: const Text(
                "Xem thêm",
                style: TextStyle(color: Color(0xFFBBBBBB)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
