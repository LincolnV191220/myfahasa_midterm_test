import 'package:flutter/material.dart';
import 'package:myfahasa/screens/home/components/section_title.dart';
import 'package:myfahasa/size_config.dart';

class SpecialOffers extends StatelessWidget {
  const SpecialOffers({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionTitle(
          text: "Ưu đãi",
          press: () {},
        ),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SaleCard(
                image:
                    "https://cdn0.fahasa.com/media/magentothem/banner7/bigsale-resize_840.jpg",
                text1: "Sale",
                text2: "Rạng rỡ",
                press: () {},
              ),
              SaleCard(
                image:
                    "https://cdn0.fahasa.com/media/magentothem/banner7/FSD.png",
                text1: "Ưu đãi",
                text2: "PNJ",
                press: () {},
              ),
              SaleCard(
                image:
                    "https://cdn0.fahasa.com/media/magentothem/banner7/Shopeepay-T5-840x320.jpg",
                text1: "Deal hời",
                text2: "SHOPEEPAY",
                press: () {},
              ),
              SizedBox(
                width: getProportionateScreenWidth(20),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class SaleCard extends StatelessWidget {
  const SaleCard({
    Key? key,
    required this.text1,
    required this.text2,
    required this.image,
    required this.press,
  }) : super(key: key);

  final String text1, text2, image;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(242),
        height: getProportionateScreenHeight(100),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Stack(
            children: [
              Image.network(
                image,
                fit: BoxFit.cover,
              ),
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF343434).withOpacity(0.5),
                      Color(0xFF343434).withOpacity(0.15),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(15),
                    vertical: getProportionateScreenWidth(10)),
                child: Text.rich(
                  TextSpan(
                    style: TextStyle(color: Colors.white),
                    children: [
                      TextSpan(
                        text: "$text1\n",
                        style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextSpan(
                        text: "$text2",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
