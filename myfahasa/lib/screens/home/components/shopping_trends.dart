import 'package:flutter/material.dart';
import 'package:myfahasa/components/product_card.dart';
import 'package:myfahasa/models/product.dart';
import 'package:myfahasa/screens/details/details_screen.dart';
import 'package:myfahasa/screens/home/components/section_title.dart';
import 'package:myfahasa/size_config.dart';

class ShoppingTrends extends StatelessWidget {
  const ShoppingTrends({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionTitle(
          text: "Xu hướng mua sắm",
          press: () {},
        ),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                demoProducts.length,
                (index) => ProductCard(
                  product: demoProducts[index],
                  press: () => Navigator.pushNamed(
                    context,
                    DetailsScreen.routeName,
                    arguments:
                        ProductDetailsArguments(product: demoProducts[index]),
                  ),
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(20),
              )
            ],
          ),
        ),
      ],
    );
  }
}
