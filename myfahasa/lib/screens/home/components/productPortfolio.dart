import 'package:flutter/material.dart';
import 'package:myfahasa/size_config.dart';

class ProductPortfolio extends StatelessWidget {
  const ProductPortfolio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List productPort = [
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/van-hoc.png",
        "text": "Văn học"
      },
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/thieu-nhi.png",
        "text": "Thiếu nhi"
      },
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/tam-ly-ky-nang.png",
        "text": "Tâm lý kỹ năng"
      },
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/kinh-te.png",
        "text": "Kinh tế"
      },
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/sach-giao-khoa.png",
        "text": "Sách giáo khoa"
      },
      {
        "img":
            "https://cdn0.fahasa.com/media/wysiwyg/icon-menu/category/foreign-books.png",
        "text": "Foreign books"
      }
    ];
    return Column(
      children: [
        Text(
          "Danh mục sản phẩm",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
            color: Colors.black,
          ),
        ),
        SizedBox(height: getProportionateScreenHeight(18)),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(
                productPort.length,
                (index) => ProductPortCard(
                    img: productPort[index]["img"],
                    text: productPort[index]["text"],
                    press: () {}),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class ProductPortCard extends StatelessWidget {
  const ProductPortCard({
    Key? key,
    required this.img,
    required this.text,
    required this.press,
  }) : super(key: key);

  final String img, text;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: SizedBox(
        width: getProportionateScreenWidth(55),
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 1,
              child: Container(
                //padding: EdgeInsets.all(getProportionateScreenWidth(0)),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Image.network(img),
              ),
            ),
            const SizedBox(height: 5),
            Text(
              text,
              style: const TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
