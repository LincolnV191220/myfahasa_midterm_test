import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:myfahasa/components/product_card.dart';
import 'package:myfahasa/constants.dart';
import 'package:myfahasa/models/product.dart';
import 'package:myfahasa/screens/home/components/home_header.dart';
import 'package:myfahasa/screens/home/components/productPortfolio.dart';
import 'package:myfahasa/screens/home/components/section_title.dart';
import 'package:myfahasa/screens/home/components/shopping_trends.dart';
import 'package:myfahasa/screens/home/components/special_offers.dart';
import 'package:myfahasa/size_config.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenHeight(20)),
            const HomeHeader(),
            SizedBox(height: getProportionateScreenHeight(30)),
            const ProductPortfolio(),
            SizedBox(height: getProportionateScreenHeight(20)),
            const SpecialOffers(),
            SizedBox(height: getProportionateScreenHeight(30)),
            const ShoppingTrends(),
            SizedBox(height: getProportionateScreenHeight(30)),
          ],
        ),
      ),
    );
  }
}
