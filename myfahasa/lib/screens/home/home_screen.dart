import 'package:flutter/material.dart';
import 'package:myfahasa/components/custom_bottom_navbar.dart';
import 'package:myfahasa/enums.dart';
import 'package:myfahasa/screens/home/components/body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static String routeName = "/home";

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}
