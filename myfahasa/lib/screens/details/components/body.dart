import 'package:flutter/material.dart';
import 'package:myfahasa/components/default_button.dart';
import 'package:myfahasa/models/product.dart';
import 'package:myfahasa/screens/cart/cart_screen.dart';
import 'package:myfahasa/screens/details/components/color_dots.dart';
import 'package:myfahasa/screens/details/components/product_description.dart';
import 'package:myfahasa/screens/details/components/product_images.dart';
import 'package:myfahasa/screens/details/components/top_rounded_container.dart';
import 'package:myfahasa/size_config.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ProductImages(product: product),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(
                product: product,
                pressOnSeeMore: () {},
              ),
              FlatButton(
                onPressed: () =>
                    Navigator.pushNamed(context, CartScreen.routeName),
                child: Row(
                  children: const [
                    Text(
                      "Add to cart",
                      style: TextStyle(
                        backgroundColor: Colors.red,
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
