import 'package:flutter/material.dart';
import 'package:myfahasa/screens/forgot_password/components/body.dart';
import 'package:myfahasa/screens/sign_in/sign_in_screen.dart';
import 'package:myfahasa/size_config.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static String routeName = "/forgot_password";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Forgot Password"),
        leading: SizedBox(
          height: getProportionateScreenWidth(40),
          width: getProportionateScreenWidth(40),
          child: FlatButton(
            padding: EdgeInsets.zero,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            color: Colors.red,
            onPressed: () {
              Navigator.pushNamed(context, SignInScreen.routeName);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
      ),
      body: Body(),
    );
  }
}
