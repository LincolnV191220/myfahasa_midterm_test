import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:myfahasa/components/custom_surffix_icon.dart';
import 'package:myfahasa/components/default_button.dart';
import 'package:myfahasa/components/form_error.dart';
import 'package:myfahasa/constants.dart';
import 'package:myfahasa/screens/forgot_password/forgot_password_screen.dart';
import 'package:myfahasa/screens/login_success/login_success_screen.dart';
import 'package:myfahasa/size_config.dart';

class SignForm extends StatefulWidget {
  const SignForm({Key? key}) : super(key: key);

  @override
  State<SignForm> createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  static Future<User?> loginUsingEmailPassword(
      {required String email,
      required String password,
      required BuildContext context}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    User? user;
    try {
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      user = userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == "user-not-found") {
        print("No user found for that email");
      }
    }
    return user;
  }

  final _formkey = GlobalKey<FormState>();
  late String email;
  late String password;
  bool rememberme = false;
  final List<String> errors = [];
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            buildEmailFormField(),
            SizedBox(
              height: getProportionateScreenHeight(30),
            ),
            buildPasswordFormField(),
            SizedBox(
              height: getProportionateScreenHeight(30),
            ),
            Row(
              children: [
                Checkbox(
                  value: rememberme,
                  activeColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      rememberme = value!;
                    });
                  },
                ),
                const Text("Remember me"),
                const Spacer(),
                GestureDetector(
                  onTap: () => Navigator.pushNamed(
                      context, ForgotPasswordScreen.routeName),
                  child: const Text(
                    "Forgot Password",
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                ),
              ],
            ),
            FormError(errors: errors),
            SizedBox(
              height: getProportionateScreenHeight(20),
            ),
            DefaultButton(
              text: "Login",
              press: () async {
                if (_formkey.currentState!.validate()) {
                  _formkey.currentState!.save();
                  User? user = await loginUsingEmailPassword(
                      email: _emailController.text,
                      password: _passwordController.text,
                      context: context);
                  if (user != null) {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => LoginSuccessScreen()),
                    );
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: _passwordController,
      obscureText: true,
      validator: _requiredPasswordValidator,
      decoration: const InputDecoration(
        labelText: "Password",
        hintText: "Enter your password",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(
          svgIcon: "assets/icons/Lock.svg",
        ),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      validator: _requiredEmailValidator,
      decoration: const InputDecoration(
        labelText: "Email",
        hintText: "Enter your email",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(
          svgIcon: "assets/icons/Mail.svg",
        ),
      ),
    );
  }

  String? _requiredEmailValidator(String? email) {
    if (email == null || email.trim().isEmpty) {
      return 'Please enter your email';
    } else if (!emailValidatorRegExp.hasMatch(email)) {
      return 'This text should have @';
    }
    return null;
  }

  String? _requiredPasswordValidator(String? password) {
    if (password == null || password.trim().isEmpty) {
      return 'Please enter your password';
    } else if (password.length < 8) {
      return 'Password must have at least 8 characters';
    }
    return null;
  }
}
