import 'package:flutter/widgets.dart';
import 'package:myfahasa/screens/cart/cart_screen.dart';
import 'package:myfahasa/screens/details/details_screen.dart';
import 'package:myfahasa/screens/forgot_password/forgot_password_screen.dart';
import 'package:myfahasa/screens/home/home_screen.dart';
import 'package:myfahasa/screens/login_success/login_success_screen.dart';
import 'package:myfahasa/screens/profile/profile_screen.dart';
import 'package:myfahasa/screens/sign_in/sign_in_screen.dart';
import 'package:myfahasa/screens/sign_up/sign_up_screen.dart';
import 'package:myfahasa/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => const SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  SignUpScreen.routeName: (context) => const SignUpScreen(),
  HomeScreen.routeName: (context) => const HomeScreen(),
  DetailsScreen.routeName: (context) => const DetailsScreen(),
  CartScreen.routeName: (context) => const CartScreen(),
  ProfileScreen.routeName: (context) => const ProfileScreen(),
};
