import 'package:flutter/material.dart';

class Product {
  final int id;
  final String title, description, price;
  final List<String> images;
  final List<Color> colors;
  final double rating;
  final bool isFavourite, isPopular;

  Product({
    required this.id,
    required this.images,
    required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description,
  });
}

// Our demo Products

List<Product> demoProducts = [
  Product(
    id: 1,
    images: [
      "https://cdn0.fahasa.com/media/catalog/product/i/m/image_217703.jpg",
      "https://cdn0.fahasa.com/media/catalog/product/8/9/8936066689793.png",
      "https://cdn0.fahasa.com/media/catalog/product/t/h/th_n_s_h_c_-_b_a_full.jpg",
      "https://cdn0.fahasa.com/media/catalog/product/t/h/th_n_s_h_c_ng_d_ng_-_nh_2.png",
      "https://cdn0.fahasa.com/media/catalog/product/t/h/th_n_s_h_c_ng_d_ng_-_nh_1.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Thần Số Học Ứng Dụng",
    price: "64.500đ",
    description: description,
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 2,
    images: [
      "https://cdn0.fahasa.com/media/catalog/product/9/3/93-6066-690-126.jpg",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Bộ Sách Tâm Lý Học Tội Phạm",
    price: "236.160đ",
    description: description,
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 3,
    images: [
      "https://cdn0.fahasa.com/media/catalog/product/i/m/image_198737.jpg",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Nói Chuyện Là Bản Năng, Giữ...",
    price: "127.200đ",
    description: description,
    rating: 4.1,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 4,
    images: [
      "https://cdn0.fahasa.com/media/catalog/product/8/9/8936066686433.jpg",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "No Vocab - No Worries",
    price: "104.000đ",
    description: description,
    rating: 4.1,
    isFavourite: true,
  ),
];

const String description =
    "Cuốn sách sẽ cung cấp mọi thứ bạn cần để mài giũa trực giác của mình, hiểu rõ hơn những người xung quanh và thậm chí có...";
